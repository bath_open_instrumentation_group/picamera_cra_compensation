# Raspberry Pi Power Supply

You will need a PSU for the Raspberry Pi, ideally one of the official ones (which provide a decent current, and are slightly over 5v).  These can be obtained from many places, e.g. [RS Components](https://uk.rs-online.com/web/p/ac-dc-adapters/1873421/).  NB the connector on the Pi 4 is different, so if you use one of those, you'll need the USB-C version of the power supply.
