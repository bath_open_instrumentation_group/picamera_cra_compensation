# Response to reviewers
This describes our point-by-point response to the reviewer comments, and a marked-up version of the manuscript that highlights our changes.

## Creating the response to reviewers
This was written in markdown and converted using the very excellent pandoc.

## Creating the "diff" PDF
I used latexdiff to create a marked-up version of the manuscript with highlighted changes.  The steps to do this are:

```bash
git show <commit hash>:manuscript/picamera_cra_compensation.tex > manuscript/response_to_reviews/submitted_manuscript.tex
cd manuscript/response_to_reviews/
latexdiff submitted_manuscript.tex ../picamera_cra_compensation.tex > diff.tex
```

You must then:
* edit the line that references artwork at `artwork/#2` to reference `../artwork/#2` instead
* edit the bibliography line to reference `../cra_compensation`
