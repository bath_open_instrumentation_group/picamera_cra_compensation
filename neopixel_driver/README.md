# NeoPixel Driver
This folder contains an Arduino sketch.  It was run on an Arduino MEGA to control a single NeoPixel LED.  If you use a different Arduino board, you may wish to change the data pin defined in the sketch.  This firmware works together with the python script in the ``characterise_lens`` folder.
